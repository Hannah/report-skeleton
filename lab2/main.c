 #include <linux/blk-mq.h>
#include <linux/blkdev.h>
#include <linux/buffer_head.h>
#include <linux/fs.h>
#include <linux/genhd.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>


#define PARTSIZE (10 * 2048)
#define MEMSIZE1 (20 * 2048)
#define MEMSIZE2 (30 * 2048)
#define MEMSIZE (50 * 2048) // Size of Ram disk in sectors
int c = 0; //Variable for Major Number 

#define SECTOR_SIZE 512
#define MBR_SIZE SECTOR_SIZE
#define MBR_DISK_SIGNATURE_OFFSET 440
#define MBR_DISK_SIGNATURE_SIZE 4
#define PARTITION_TABLE_OFFSET 446
#define PARTITION_ENTRY_SIZE 16 
#define PARTITION_TABLE_SIZE 64 
#define MBR_SIGNATURE_OFFSET 510
#define MBR_SIGNATURE_SIZE 2
#define MBR_SIGNATURE 0xAA55
#define BR_SIZE SECTOR_SIZE
#define BR_SIGNATURE_OFFSET 510
#define BR_SIGNATURE_SIZE 2
#define BR_SIGNATURE 0xAA55

typedef struct
{
	unsigned char boot_type; // 0x00 - Inactive; 0x80 - Active (Bootable)
	unsigned char start_head;
	unsigned char start_sec:6;
	unsigned char start_cyl_hi:2;
	unsigned char start_cyl;
	unsigned char part_type;
	unsigned char end_head;
	unsigned char end_sec:6;
	unsigned char end_cyl_hi:2;
	unsigned char end_cyl;
	unsigned int abs_start_sec;
	unsigned int sec_in_part;
} PartEntry;

typedef PartEntry PartTable[4];

#define SEC_PER_HEAD 63
#define HEAD_PER_CYL 255
#define HEAD_SIZE (SEC_PER_HEAD * SECTOR_SIZE)
#define CYL_SIZE (SEC_PER_HEAD * HEAD_PER_CYL * SECTOR_SIZE)

#define sec4size(s) ((((s) % CYL_SIZE) % HEAD_SIZE) / SECTOR_SIZE)
#define head4size(s) (((s) % CYL_SIZE) / HEAD_SIZE)
#define cyl4size(s) ((s) / CYL_SIZE)


static PartTable def_part_table =
{
	{
		boot_type: 0x00,
		start_sec: 0x0,
		start_head: 0x0,
		start_cyl: 0x0,
		part_type: 0x83,
		end_head: head4size(MEMSIZE1 - 1) + 1,
		end_sec: sec4size(MEMSIZE1 - 1),
		end_cyl: cyl4size(MEMSIZE - 1), // &0xFF??
		//end_cyl_hi??
		abs_start_sec: 0x1,
		sec_in_part: MEMSIZE1 // 20Mbyte
	},
	//extended
	{
		boot_type: 0x00,
		start_head: head4size(MEMSIZE1),
		start_sec: sec4size(MEMSIZE1) + 1,
		start_cyl: cyl4size(MEMSIZE1),
		part_type: 0x05, // extended partition type
		end_sec: sec4size(MEMSIZE1 + MEMSIZE2 - 1) + 1,
		end_head: head4size(MEMSIZE1 + MEMSIZE2 - 1),
		end_cyl: cyl4size(MEMSIZE1 + MEMSIZE2 - 1), //&0xFF??
		//end_cyl_hi??
		abs_start_sec: MEMSIZE1 + 1,
		sec_in_part: MEMSIZE2 //30Mbyte
	}
};
static unsigned int def_log_part_br_abs_start_sector[] = {MEMSIZE1 + 1, MEMSIZE1 + PARTSIZE + 1, MEMSIZE1 + PARTSIZE + PARTSIZE + 1};
static const PartTable def_log_part_table[] =
{
	{
		{
			boot_type: 0x00,
			start_head: 0x00,
			start_sec: 0x0, 
			start_cyl: 0x0, 
			part_type: 0x83,
			end_head: head4size(PARTSIZE - 1),
			end_sec: sec4size(PARTSIZE - 1) ,
			end_cyl: cyl4size(PARTSIZE - 1),
			abs_start_sec: 0x0,
			sec_in_part: PARTSIZE
		},
		
		{
			boot_type: 0x00,
			start_head: head4size(PARTSIZE) + 1,
			start_sec: sec4size(PARTSIZE),
			start_cyl: cyl4size(PARTSIZE),
			part_type: 0x05,
			end_head: head4size(PARTSIZE + PARTSIZE - 1) + 1,
			end_sec: sec4size(PARTSIZE + PARTSIZE - 1),
			end_cyl: cyl4size(PARTSIZE + PARTSIZE - 1),
			abs_start_sec: PARTSIZE,
			sec_in_part: PARTSIZE + 1
		}
	},
	{
		{
			boot_type: 0x00,
			start_head: 0x00,
			start_sec: 0x00, 
			start_cyl: 0x0, 
			part_type: 0x83,
			end_head: head4size(PARTSIZE - 1),
			end_sec: sec4size(PARTSIZE - 1),
			end_cyl: cyl4size(PARTSIZE - 1),
			abs_start_sec: 0x0,
			sec_in_part: PARTSIZE
		},
		
		{
			boot_type: 0x00,
			start_head: head4size(PARTSIZE + PARTSIZE) + 1,
			start_sec: sec4size(PARTSIZE + PARTSIZE),
			start_cyl: cyl4size(PARTSIZE + PARTSIZE),
			part_type: 0x05,
			end_head: head4size(PARTSIZE + PARTSIZE + PARTSIZE - 1) + 1,
			end_sec: sec4size(PARTSIZE + PARTSIZE + PARTSIZE - 1),
			end_cyl: cyl4size(PARTSIZE + PARTSIZE + PARTSIZE - 1),
			abs_start_sec: PARTSIZE + PARTSIZE ,
			sec_in_part: PARTSIZE + 1
		}
	},
	{
		{
			boot_type: 0x00,
			start_head: 0x00,
			start_sec: 0x0, 
			start_cyl: 0x0, 
			part_type: 0x83,
			end_head: head4size(PARTSIZE - 1),
			end_sec: sec4size(PARTSIZE - 1) + 1,
			end_cyl: cyl4size(PARTSIZE - 1),
			abs_start_sec: 0x0,
			sec_in_part: PARTSIZE
		}
	}
};




static void copy_mbr(u8 *disk)
{
	memset(disk, 0x0, MBR_SIZE);
	//*(unsigned long *)(disk + MBR_DISK_SIGNATURE_OFFSET) = 0x36E5756D;
	memcpy(disk + PARTITION_TABLE_OFFSET, &def_part_table, PARTITION_TABLE_SIZE);
	*(unsigned short *)(disk + MBR_SIGNATURE_OFFSET) = MBR_SIGNATURE;
}
static void copy_br(u8 *disk, int abs_start_sector, const PartTable *part_table)
{
	disk += (abs_start_sector * SECTOR_SIZE);
	memset(disk, 0x0, BR_SIZE);
	memcpy(disk + PARTITION_TABLE_OFFSET, part_table,
		PARTITION_TABLE_SIZE);
	*(unsigned short *)(disk + BR_SIGNATURE_OFFSET) = BR_SIGNATURE;
}
void copy_mbr_n_br(u8 *disk)
{
	int i;

	copy_mbr(disk);
	for (i = 0; i < ARRAY_SIZE(def_log_part_table); i++)
	//for (i = 0; i < 5; i++)
	{
		copy_br(disk, def_log_part_br_abs_start_sector[i], &def_log_part_table[i]);
	}
}


/* Structure associated with Block device*/
struct mydiskdrive_dev 
{
	int size;
	u8 *data;
	spinlock_t lock;
	struct blk_mq_tag_set tag_set;
	struct request_queue *queue;
	struct gendisk *gd;

}device;

struct mydiskdrive_dev *x;

static int my_open(struct block_device *x, fmode_t mode)	 
{
	int ret=0;
	printk(KERN_INFO "mydiskdrive : open \n");
	return ret;

}

static void my_release(struct gendisk *disk, fmode_t mode)
{
	printk(KERN_INFO "mydiskdrive : closed \n");
}

static struct block_device_operations fops =
{
	.owner = THIS_MODULE,
	.open = my_open,
	.release = my_release,
};

int mydisk_init(void)
{
	(device.data) = vmalloc(MEMSIZE * SECTOR_SIZE);
	/* Setup its partition table */
	
	if(device.data == NULL)
		return -1;
	copy_mbr_n_br(device.data);

	return MEMSIZE;	
}

static int rb_transfer(struct request *req, unsigned int *nr_bytes)
{
	int dir = rq_data_dir(req);
	int ret = 0;
	/*starting sector
	 *where to do operation*/
	sector_t start_sector = blk_rq_pos(req);
	unsigned int sector_cnt = blk_rq_sectors(req); /* no of sector on which opn to be done*/
	struct bio_vec bv;
	#define BV_PAGE(bv) ((bv).bv_page)
	#define BV_OFFSET(bv) ((bv).bv_offset)
	#define BV_LEN(bv) ((bv).bv_len)
	struct req_iterator iter;
	sector_t sector_offset;
	unsigned int sectors;
	u8 *buffer;
	sector_offset = 0;
	rq_for_each_segment(bv, req, iter)
	{
		buffer = page_address(BV_PAGE(bv)) + BV_OFFSET(bv);
		if (BV_LEN(bv) % (SECTOR_SIZE) != 0)
		{
			printk(KERN_ERR"bio size is not a multiple ofsector size\n");
			ret = -EIO;
		}
		sectors = BV_LEN(bv) / SECTOR_SIZE;
		
		/*printk(KERN_DEBUG "my disk: Start Sector: %llu, Sector Offset: %llu;\
		Buffer: %p; Length: %u sectors\n",\
		(unsigned long long)(start_sector), (unsigned long long) \
		(sector_offset), buffer, sectors);*/
		
		if (dir == WRITE) /* Write to the device */
		{
			memcpy((device.data)+((start_sector+sector_offset)*SECTOR_SIZE)\
			,buffer,sectors*SECTOR_SIZE);		
		}
		else /* Read from the device */
		{
			memcpy(buffer,(device.data)+((start_sector+sector_offset)\
			*SECTOR_SIZE),sectors*SECTOR_SIZE);	
		}
		sector_offset += sectors;
		*nr_bytes += BV_LEN(bv);
		
	}
	
	if (sector_offset != sector_cnt)
	{
		printk(KERN_ERR "mydisk: bio info doesn't match with the request info");
		ret = -EIO;
	}
	return ret;
}

static blk_status_t queue_rq(struct blk_mq_hw_ctx *hctx,
        const struct blk_mq_queue_data *bd)
{
  unsigned int nr_bytes = 0;
  blk_status_t status = BLK_STS_OK;

  blk_mq_start_request(bd->rq);

  if ((rb_transfer(bd->rq, &nr_bytes) != 0 )
  || blk_update_request(bd->rq, status, nr_bytes))
    status = BLK_STS_IOERR;

  blk_mq_end_request(bd->rq, status);
  return status;
}

static struct blk_mq_ops mq_ops = {.queue_rq = queue_rq,};

void mydisk_cleanup(void)
{
	vfree(device.data);
}

static int device_setup(void)
{
	if (mydisk_init() == -1){
		printk("Coudn't init mydisk...\n");
		return -1;
	};
	if((c = register_blkdev(c, "mydisk")) < 0){
		printk("Coudn't register blk_dev... Clean disk\n");
		mydisk_cleanup();
		return -1;
	};// major no. allocation
	printk(KERN_ALERT "Major Number is : %d",c);
	spin_lock_init(&device.lock); // lock for queue
	if(!(device.queue = blk_mq_init_sq_queue(&device.tag_set, &mq_ops, 128, BLK_MQ_F_SHOULD_MERGE))){
		printk("Couldn't init queue... Unreg blok_dev and clean disk...\n");
		unregister_blkdev(c, "mydisk");
		mydisk_cleanup();
		return -1;
	}; 

	if(!(device.gd = alloc_disk(8))){
		printk("Couldn't alloc disk... I'll just clean everything...\n");
		blk_cleanup_queue(device.queue);
		unregister_blkdev(c, "mydisk");
		mydisk_cleanup();
		return -1;
	}; // gendisk allocation
	
	(device.gd)->major=c; // major no to gendisk
	device.gd->first_minor=0; // first minor of gendisk

	device.gd->fops = &fops;
	device.gd->private_data = &device;
	device.gd->queue = device.queue;
	device.size= mydisk_init();
	printk(KERN_INFO"THIS IS DEVICE SIZE %d",device.size);	
	sprintf(((device.gd)->disk_name), "mydisk");
	set_capacity(device.gd, device.size);  
	add_disk(device.gd);
	printk("Everything is ok. I've done everything");
	return 0;
}

static int __init mydiskdrive_init(void)
{	

	int ret = device_setup();
	
	return ret;
}

void __exit mydiskdrive_exit(void)
{	
	del_gendisk(device.gd);
	put_disk(device.gd);
	blk_cleanup_queue(device.queue);
	unregister_blkdev(c, "mydisk");
	mydisk_cleanup();	
}

module_init(mydiskdrive_init);
module_exit(mydiskdrive_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Hannah, Oleja");
MODULE_DESCRIPTION("IO systems lab 2");

