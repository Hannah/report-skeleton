#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/proc_fs.h>
#include <linux/init.h>
#include <linux/uaccess.h>


 
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Kurashov Oleg, Savon Galina P33301");
MODULE_DESCRIPTION("The first kernel module");

static dev_t first;
static struct cdev c_dev; 
static struct class *cl;
static struct proc_dir_entry* entry;

static int my_open(struct inode *i, struct file *f)
{
  printk(KERN_INFO "Driver: open()\n");
  return 0;
}

static int my_close(struct inode *i, struct file *f)
{
  printk(KERN_INFO "Driver: close()\n");
  return 0;
}

static char c;
static int k;
static int pointer = 0;
static char text_k[5500];

static ssize_t dev_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{
  size_t len1 = strlen(text_k);
  if(*off > 0|| len < len1){
     return 0;
  }
  printk(KERN_INFO "%s", text_k);
  *off = len1;
  return 1;
}

static ssize_t proc_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{
  size_t len1 = strlen(text_k);
  if(*off > 0 || len < len1){
  	return 0;
  }
  if(copy_to_user(buf, text_k, len1) != 0)
  	return -EFAULT;
  *off = len1;
  return len1;
}

static ssize_t dev_write(struct file *f, const char __user *buf,  size_t len, loff_t *off)
{

  printk(KERN_INFO "Driver: write()\n");
  k = 0;
  for(int i = 0; i < len; i++){
  	if((buf[i] <= 'z' && buf[i] >= 'a') || (buf[i] <= 'Z' && buf[i] >= 'A')){
  		k++;
  	}
  }
  pointer++;
  sprintf(text_k + strlen(text_k), "enter %d : %d\n", pointer, k);
  return len;
}

static struct file_operations mychdev_fops =
{
  .owner = THIS_MODULE,
  .read = dev_read,
  .write = dev_write
};
 
static struct proc_ops fops = {
	.proc_read = proc_read,
};
 
 
static int __init ch_drv_init(void)
{
    printk(KERN_INFO "Hello!\n");
    //proc_init
    if(entry = proc_create("var5", 0444, NULL, &fops) == NULL){
		return -1;
	}
	
    if (alloc_chrdev_region(&first, 0, 1, "ch_dev") < 0)
	  {  	
    		proc_remove(entry);
		return -1;
	  }
    if ((cl = class_create(THIS_MODULE, "chardrv")) == NULL)
	  {
		unregister_chrdev_region(first, 1);
		proc_remove(entry);
		return -1;
	  }
    if (device_create(cl, NULL, first, NULL, "mychdev") == NULL)
	  {
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		proc_remove(entry);
		return -1;
	  }
    cdev_init(&c_dev, &mychdev_fops);
    if (cdev_add(&c_dev, first, 1) == -1)
	  {
		device_destroy(cl, first);
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
    		proc_remove(entry);
		return -1;
	  }
    return 0;
}
 
static void __exit ch_drv_exit(void)
{
    cdev_del(&c_dev);
    proc_remove(entry);
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    printk(KERN_INFO "Bye!!!\n");
}
 
module_init(ch_drv_init);
module_exit(ch_drv_exit);


