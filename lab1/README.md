#TODO
ioctl-запрос при запросе клиентским приложением. придумать сообщение. и вернуть инфу из прока

# Лабораторная работа 1

**Название:** "Разработка драйверов символьных устройств"

**Цель работы:** получить знания и навыки разработки драйверов символьных
устройств для операционной системы Linux.

## Описание функциональности драйвера

При записи текста в файл символьного устройства должно запоминаться количество введенных букв. Последовательность полученных результатов с момента загрузки модуля ядра должна выводиться при чтении созданного файла /proc/varN в консоль пользователя.

При чтении из файла символьного устройства в кольцевой буфер ядра должен осуществляться вывод тех же данных,которые выводятся при чтении файла /proc/varN.

## Инструкция по сборке

make
insmod ch_dev.ko

## Инструкция пользователя

1.Ведите в файл /dev/mychdev текст

2.При чтении из файла /proc/var5 получите результат работы

3.Либо при чтении из /dev/mychdev и вызове dmesg

## Примеры использования

root@hannah-VirtualBox:/dev# echo "Hello, world!" > mychdev

root@hannah-VirtualBox:/dev# cat ../proc/var5

enter 1 number of letters = 10

root@hannah-VirtualBox:/dev# cat mychdev

root@hannah-VirtualBox:/dev# dmesg

...

[  645.278066] Bye!!!

[  647.985450] Hello!

[  654.360245] Driver: write()

[  657.049977] enter 1 : 10


root@hannah-VirtualBox:/dev# echo "It's me!" > mychdev

root@hannah-VirtualBox:/dev# cat mychdev

...

[  662.301782] Driver: write()

[  664.323471] enter 1 : 10

               enter 2 : 5
               

root@hannah-VirtualBox:/dev# echo "It's 4.40 AM now" > mychdev

root@hannah-VirtualBox:/dev# cat ../proc/var5

enter 1 : 10

enter 2 : 5

enter 3 : 8


